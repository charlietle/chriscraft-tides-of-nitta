//
//  Tile.swift
//  ChristCraft: Tides of Nitta
//
//  Created by Charlie on 10/29/15.
//  Copyright © 2015 UCDClassNitta. All rights reserved.
//

import Foundation
import SpriteKit

let TERRAIN_FILENAME    = "Terrain"
let TERRAIN_FILEEXT     = "dat"
let TERRAIN_DELIM       = "\n"

// The lines for which the info can be found in the .dat file
let TERRAIN_INDEX_SPRITE_FILENAME = 0
let TERRAIN_INDEX_SPRITE_COUNT = 1
let TERRAIN_INDEX_SPRITENAMES_START = 2

// What the sprite sheet has for tile sizes.
let TILE_WIDTH = CGFloat(32)
let TILE_HEIGHT = CGFloat(32)

// Experimentally found by loading spritesheets of different heights
let MAX_SPRITESHEET_HEIGHT = CGFloat(4064.0)

enum TileType: String {
    case None, Grass, Dirt, Rock, Tree, Stump, Water, Wall, WallDamaged, Rubble, Max
}

class Tile: SKSpriteNode {
    var type: TileType
    var typeTag: String
    static var terrainSpriteSheets: [String: SKTexture?] = [:]
    static var terrainSpriteDict: [String: Int] = [:]
//    MARK: Initializers
    
//    Initializer
    init(tileType: TileType, tag: String) {
        type = tileType
        typeTag = tag
        let texture = Tile.textureFromTerrain(tileType, tag: typeTag)
        super.init(texture: texture, color: UIColor.whiteColor(), size: CGSizeMake(TILE_WIDTH, TILE_HEIGHT))
        anchorPoint = CGPointZero
    }

//    Superclass requires this implementation, but we'll safely ignore it for now.
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    static func stringForTileType(tileType: TileType) -> String {
        switch tileType {
        case .WallDamaged:
            return "wall-damaged"
        default:
            return tileType.rawValue.lowercaseString
        }
    }
    
    static func loadTerrain() {
        let resourceURL = NSBundle.mainBundle().URLForResource(TERRAIN_FILENAME, withExtension: TERRAIN_FILEEXT)
        
        let terrainSpriteStr = try! String(contentsOfURL: resourceURL!, encoding: NSUTF8StringEncoding)
        let terrainSpriteArr: [String] = terrainSpriteStr.componentsSeparatedByString(TERRAIN_DELIM)
        let terrainSpriteCount = Int(terrainSpriteArr[TERRAIN_INDEX_SPRITE_COUNT])!
        
        for index in TERRAIN_INDEX_SPRITENAMES_START..<(TERRAIN_INDEX_SPRITENAMES_START + terrainSpriteCount) {
            terrainSpriteDict[terrainSpriteArr[index]] = index - TERRAIN_INDEX_SPRITENAMES_START
        }
    }
    
//    MARK: Sprite Finder for Tile
//    Should return the texture for the sprite.
    static func textureFromTerrain(tileType: TileType, tag: String) -> SKTexture {

        if terrainSpriteDict.isEmpty {
            loadTerrain()
        }
        
        let terrainSpriteName: String = Tile.stringForTileType(tileType) + "-" + tag
        var terrainSpriteIndex: Int
        if terrainSpriteDict[terrainSpriteName] == nil {
            terrainSpriteIndex = terrainSpriteDict[Tile.stringForTileType(tileType) + "-0"]!
        } else {
            terrainSpriteIndex = terrainSpriteDict[terrainSpriteName]!
        }
        
        let terrainSpriteSheetSpritesPerPartition = Int(MAX_SPRITESHEET_HEIGHT / TILE_HEIGHT)
        let terrainSpriteSheetPartition = terrainSpriteIndex / terrainSpriteSheetSpritesPerPartition
        let terrainSpriteSheetImageName = TERRAIN_FILENAME + String(terrainSpriteSheetPartition)
        
        if Tile.terrainSpriteSheets[terrainSpriteSheetImageName] == nil {
            Tile.terrainSpriteSheets[terrainSpriteSheetImageName] = SKTexture(imageNamed: terrainSpriteSheetImageName)
        }
        
        let terrainSpriteSheet = Tile.terrainSpriteSheets[terrainSpriteSheetImageName]!
        let terrainSpriteTileIndexPercentage = CGFloat((terrainSpriteIndex % terrainSpriteSheetSpritesPerPartition) + 1) / CGFloat(terrainSpriteSheetSpritesPerPartition)
        let tileHeightPercentage = 1 / CGFloat(terrainSpriteSheetSpritesPerPartition)

        let terrainTileSprite = SKTexture(rect: CGRectMake(0, 1-terrainSpriteTileIndexPercentage, 1, tileHeightPercentage), inTexture: terrainSpriteSheet!)
        
        print(terrainTileSprite)
        
        return terrainTileSprite
    }
    
///    MARK: Helper Functions
//    Gets the type for a string in the type
    static func typeForString(typeStr: String) -> TileType {
        switch typeStr {
        case "G":
            return .Grass
        case "D":
            return .Dirt
        case "R":
            return .Rock
        case "F":
            return .Tree
        case "S":
            return .Stump
        case " ":
            return .Water
        case "w":
            return .Wall
        case "W":
            return .WallDamaged
        case "R":
            return .Rubble
        default:
            return .None
        }
    }

}