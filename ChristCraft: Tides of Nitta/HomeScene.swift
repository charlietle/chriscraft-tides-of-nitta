//
//  HomeScene.swift
//  ChristCraft: Tides of Nitta
//
//  Created by Charlie on 10/23/15.
//  Copyright © 2015 UCDClassNitta. All rights reserved.
//

import SpriteKit

class HomeScene: SKScene {
    
    override func didMoveToView(view: SKView) {
        super.didMoveToView(view)

    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        let touchedNode = nodeAtPoint(touches.first!.locationInNode(self))
        
        if let name = touchedNode.name {
            if name == "ProceedToMapSelection" || name == "SinglePlayer" {
                if let scene = MapSelectionScene(fileNamed:"MapSelectionScene") {
                    scene.scaleMode = .Fill
                    self.view!.presentScene(scene)
                }
            }
        }
        
    }
   
}