//
//  Map.swift
//  ChristCraft: Tides of Nitta
//
//  Created by Charlie on 11/1/15.
//  Copyright © 2015 UCDClassNitta. All rights reserved.
//

import Foundation
import SpriteKit

class Map: SKNode {
    let MAP_FILENAME = "2player"
    let MAP_FILEEXT = "map"
    let MAP_DELIM = "\n"
    
    enum MAP_FILE_INDEX : Int {
        case NAME = 0
        case DIMENSIONS
        case PLACEMENTS
    }
    
    enum DIMENSIONS_INDEX: Int {
        case WIDTH = 0
        case HEIGHT
    }
    
    var camera = SKNode()
    var mapTilePlacements: [String] = []
    var size: CGSize = CGSizeZero
    
    override init() {
        super.init()
        addChild(camera)
        loadMap()
        loadTiles()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func centerOnCamera() {
        let cameraPositionInScene = scene!.convertPoint(camera.position, fromNode: self)
        position = CGPointMake(position.x - cameraPositionInScene.x, position.y - cameraPositionInScene.y)
    }
    
//    MARK: File Parsing
//    Map loading the .map file for tile rendering
    func loadMap() {
        let resourceURL = NSBundle.mainBundle().URLForResource(MAP_FILENAME, withExtension: MAP_FILEEXT)
        let mapFileStr = try! String(contentsOfURL: resourceURL!, encoding: NSUTF8StringEncoding)
        let mapFileArr = mapFileStr.componentsSeparatedByString(MAP_DELIM)
        
        name = mapFileArr[MAP_FILE_INDEX.NAME.rawValue]
        let dimensions = mapFileArr[MAP_FILE_INDEX.DIMENSIONS.rawValue].componentsSeparatedByString(" ")
        size = CGSizeMake(CGFloat(Int(dimensions[DIMENSIONS_INDEX.WIDTH.rawValue])!), CGFloat(Int(dimensions[DIMENSIONS_INDEX.HEIGHT.rawValue])!))
        for row in MAP_FILE_INDEX.PLACEMENTS.rawValue...MAP_FILE_INDEX.PLACEMENTS.rawValue + Int(size.height) + 1 {
            mapTilePlacements.append(mapFileArr[row])
        }
//        The loadTiles() function will load the tiles from the bottom of the file to the top.
        mapTilePlacements = mapTilePlacements.reverse()
        print(mapTilePlacements)
    }
    

///    MARK: Loading the tiles
//    Adds the tiles to the screen.
    func loadTiles() {
        var row: Int = 0
        var col: Int = 0
        
        for rowStr in mapTilePlacements {
            for colStr in rowStr.characters {
                let tileType = Tile.typeForString("\(colStr)")
                let tile = Tile(tileType: tileType, tag: "a")
                tile.position = CGPointMake(CGFloat(col) * TILE_WIDTH, CGFloat(row) * TILE_HEIGHT)
                addChild(tile)
                col += 1
            }
            row += 1
            col = 0
        }
    }
}