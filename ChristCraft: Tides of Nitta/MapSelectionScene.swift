//
//  MapSelectionScene.swift
//  ChristCraft: Tides of Nitta
//
//  Created by Charlie on 10/24/15.
//  Copyright © 2015 UCDClassNitta. All rights reserved.
//

import Foundation
import SpriteKit

class MapSelectionScene: SKScene {
    override func didMoveToView(view: SKView) {
        super.didMoveToView(view)
        
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        let touchedNode = nodeAtPoint(touches.first!.locationInNode(self))
        if let name = touchedNode.name {
            if name == "ProceedToGame" || name == "SinglePlayer" {
                if let scene = GameScene(fileNamed: "GameScene") {
                    scene.scaleMode = .Fill
                    view?.presentScene(scene)
                }
            } else if name == "BackToMenu" || name == "cancelLabel" {
                if let scene = HomeScene(fileNamed: "HomeScene") {
                    scene.scaleMode = .Fill
                    view?.presentScene(scene)
                }
            }
        }
        
    }
}