//
//  GameScene.swift
//  ChristCraft: Tides of Nitta
//
//  Created by Charlie on 10/22/15.
//  Copyright (c) 2015 UCDClassNitta. All rights reserved.
//

import SpriteKit

class GameScene: SKScene {
    
    var map = Map()
    
    var lastTouchPosition = CGPointZero
    
    override func didMoveToView(view: SKView) {
        /* Setup your scene here */
        addChild(map)
        super.didMoveToView(view)
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
       /* Called when a touch begins */
        lastTouchPosition = touches.first!.locationInNode(self)
    }
    
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        let newTouchPosition = touches.first!.locationInNode(self)
        let touchOffsetVector = CGPointMake(newTouchPosition.x - lastTouchPosition.x, newTouchPosition.y - lastTouchPosition.y)
        let mapCameraPositionInScene = convertPoint(map.camera.position, toNode: self)
        
//        We want to limit the x range from [0, map width * tile_width - frame width]
//        The frame width is subtracted because the scene anchor point is at 0,0.
//        Without the subtraction, we'd see an entire frame's worth of blank space.
//        The same goes for the acceptables ranges of y values
        map.camera.position = CGPointMake(max(min(mapCameraPositionInScene.x - touchOffsetVector.x, (map.size.width + 2.0) * TILE_WIDTH - frame.size.width), 0),
                                          max(min(mapCameraPositionInScene.y - touchOffsetVector.y, (map.size.height + 2.0) * TILE_HEIGHT - frame.size.height), 0))
        lastTouchPosition = newTouchPosition
    }
   
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
    }
    
    override func didFinishUpdate() {
        map.centerOnCamera()
    }
}
